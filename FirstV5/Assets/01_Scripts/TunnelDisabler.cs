﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelDisabler : MonoBehaviour
{
    public GameObject toDisable;
    int playersCount;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        playersCount++;
        if (playersCount == 4)
            toDisable.SetActive(false);
    }
}