﻿using UnityEngine;

[CreateAssetMenu(fileName = "FloatName", menuName = "ScriptableObjects/Float")]
public class FloatObject : ScriptableObject
{
    public float Value;
}