﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Reload : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetButtonDown("Validation"))
        {
            SceneManager.LoadScene("StartScene");
        }
    }
}